<?php

$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'petdb');
 
if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

if(isset($_POST['speices']) && isset($_POST['name']) && isset($_POST['weight']) && isset($_POST['description']) && isset($_POST['picture'])) {
	$species = $_POST['speices'];
    $name = $_POST['name'];
    $weight = (float)$_POST['weight'];
    $description = $_POST['description'];
    $picture = $_FILES['picture'];
    $path_parts = pathinfo($picture);
    $filename = $path_parts['basename'];

    
    
    $stmt=$mysqli->prepare("INSERT INTO petdb (id, species, name, weight, description, filename) VALUES ('', ?, ?, ?, ?, ?)");
	if(!$stmt) {
		printf("Query Prep Failed: %s\n", $mysqli->error);
	    exit();
	}
	
	$stmt->bind_param('ssdss',$species, $name, $weight, $description, $filename);
	
	if (!$stmt->execute()) {
	    echo $mysqli->error;
	    $stmt->close();
	} else {
        
        $full_path = sprintf("/var/www");
        
        if( move_uploaded_file($_FILES['picture']['tmp_name'], $full_path) ){
            echo 'Upload suceeded';
            header("Location: pet-listings.php");
            exit;
        }else{
            echo 'Upload failed';
            exit;
        }  
        
        $stmt->close();
	}
    
} else {
    echo "invalid input values";
}




?>


