<?php

$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'petdb');
 
if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

?>

<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Pet Listings</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
    <h1>Pet Listings</h1>
</head>
<body><div id="main">
 
 
 <p>
<?php
			$stmt = $mysqli->prepare("SELECT id, species FROM pets");
			if(!$stmt) {
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit();
			}
		
			$stmt->execute();
			$stmt->bind_result($id, $species);
			$stmt->store_result();
			while ($stmt->fetch()) {
                echo htmlentities($species)."<br>".htmlentities($id), "\n";
			}
			$stmt->close();

?>

<?php
			$stmt = $mysqli->prepare("SELECT species, name, weight, description, filename FROM pets");
			if(!$stmt) {
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit();
			}
		
			$stmt->execute();
			$stmt->bind_result($species, $name, $weight, $description, $filename);
			$stmt->store_result();
			while ($stmt->fetch()) {
                ?>
                <div>
                    <img src="/var/www/".<?php echo htmlentities($filename) ?>>
                    <ul>
                        <li>name : <?php echo htmlentities($name) ?> </li>
                        <li>species : <?php echo htmlentities($species) ?> </li>
                        <li>weight : <?php echo htmlentities($weight) ?> </li>
                        <li>description : <?php echo htmlentities($description) ?> </li>
                    </ul>
                </div>
                <?php
			}
			$stmt->close();

?>




 </p>
<!-- CONTENT HERE -->
 
</div></body>
</html>
